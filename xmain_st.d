// main driver
module xmain/* is aliced*/;

private:
import core.time;

import glbinds;
import glutils;


// ////////////////////////////////////////////////////////////////////////// //
//#extension GL_ARB_compatibility : enable
enum ShaderPre = q{
#version 120
uniform vec3 iResolution;            // viewport resolution (in pixels)
uniform vec4 iMouse;                 // mouse pixel coords
uniform float iGlobalTime;           // shader playback time (in seconds)
uniform float iGlobalFrame;          // ???
uniform float iTimeDelta;            // how long last frame took to render, in seconds (TODO)
uniform float iFrame;                // buffer shaders seems to have this
uniform vec3 iChannelResolution[4];  // channel resolution (in pixels)
uniform float iChannelTime[4];       // channel playback time (in sec)
uniform vec4 iDate;                  // (year, month, day, time in seconds)
//uniform float iGlobalDelta;          // i don't know
//uniform float iSampleRate;           // sound sample rate (i.e., 44100)
};

enum ShaderMain = q{
void main (void) {
  vec2 fc = vec2(gl_FragCoord.x, gl_FragCoord.y);
  mainImage(gl_FragColor, fc);
  gl_FragColor.w = 1.0;
}
};

// ////////////////////////////////////////////////////////////////////////// //
import arsd.color;
import arsd.png;


__gshared SimpleWindow sdwindow;


public enum vlWidth = 800;
public enum vlHeight = 500;
public enum scale = 1;

public enum vlEffectiveWidth = vlWidth*scale;
public enum vlEffectiveHeight = vlHeight*scale;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string basePath;
__gshared string shaderFile;
__gshared string shaderSource;
__gshared Texture[4] textures; // 4 texture samplers
__gshared TextureCube[4] texturesCube; // 4 cube texture samplers (can be null)
__gshared Texture texMain;
__gshared Shader shader;
__gshared Shader[4] shaderbufs; // a,b,c,d
__gshared int[4] bufmap = -1; // bufmap[texnum] == bufN (or -1)
__gshared FBO[4] buffbos;
__gshared GLuint listQuad;

// ////////////////////////////////////////////////////////////////////////// //
void initOpenGL () {
  static auto loadShader (string sname, string src) {
    string spre = ShaderPre~"\n";
    // setup texture samplers
    foreach (int idx; 0..4) {
      import std.string : format;
      if (texturesCube[idx] !is null) {
        spre ~= "uniform samplerCube iChannel%s;\n".format(idx);
      } else {
        spre ~= "uniform sampler2D iChannel%s;\n".format(idx);
      }
    }
    spre ~= "#line 0\n";
    auto shader = new Shader(sname, spre~src~"\n"~ShaderMain);
    shader.exec({
      shader["iResolution"] = SVec3F(vlWidth, vlHeight, 0.0f);
      foreach (int idx; 0..4) {
        char[9] vname = "iChannel0";
        vname[$-1] = cast(char)('0'+idx);
        int bufidx = bufmap[idx];
        if (bufidx < 0) bufidx = idx;
        shader[vname[]] = cast(int)bufidx;
      }
      auto cri = shader.varId("iChannelResolution");
      float[3][4] cres = void;
      foreach (int idx; 0..4) {
        if (texturesCube[idx]) {
          cres[idx][0] = texturesCube[idx].width;
          cres[idx][1] = texturesCube[idx].height;
        } else {
          int bufidx = bufmap[idx];
          if (bufidx < 0) bufidx = idx;
          cres[idx][0] = textures[bufidx].width;
          cres[idx][1] = textures[bufidx].height;
        }
        cres[idx][2] = 0;
      }
      glUniform3fv(cri, 4, &cres[0][0]);
    });
    return shader;
  }

  glEnable(GL_TEXTURE_2D);
  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);

  // load textures
  try {
    import std.path;
    import std.stdio : File;
    int num = 0;
    foreach (/*immutable*/ line; File(shaderFile.setExtension(".tex")).byLine) {
      Texture tex;
      bufmap[num] = -1;
      if (line == "bufa" || line == "bufb" || line == "bufc" || line == "bufd") {
        // render buffer
        int bufidx = line[3]-'a';
        bufmap[num] = bufidx;
        tex = new Texture(vlWidth, vlHeight, true, Texture.Option.fp, Texture.Option.nearest, Texture.Option.clamp); // it's floating point, i guess
      } else if (line.length > 4 && line[0..4] == "cube") {
        // load cubemap texture
        import std.string : format;
        try {
          string fn = basePath~"/textures/cube/%s_%%s.png".format(line);
          auto texc = new TextureCube(fn);
          texturesCube[num++] = texc;
          if (num >= textures.length) break;
          continue;
        } catch (Exception e) {
          import std.stdio;
          writeln("can't load cube texture '", line, "'");
          assert(0);
          //tex = null;
        }
      } else {
        import std.string : format;
        try {
          string fn = basePath~"/textures/tex%s.png".format(line);
          tex = new Texture(fn);
        } catch (Exception e) {
          import std.stdio;
          writeln("can't load texture '", line, "'");
          tex = null;
        }
      }
      textures[num++] = tex;
      if (num >= textures.length) break;
    }
  } catch (Exception e) {}
  //foreach (immutable num; 0..4) if (textures[num] is null) textures[num] = new Texture(512, 512);
  foreach (immutable num; 0..4) {
    if (textures[num] is null && texturesCube[num] is null) {
      //textures[num] = new Texture(basePath~"/textures/tex00.png");
      textures[num] = new Texture(vlWidth, vlHeight, true/*, Texture.Option.fp*/);
    }
  }
  texMain = new Texture(vlWidth, vlHeight, true);

  foreach (int idx; 0..4) {
    glActiveTexture(GL_TEXTURE0+idx);
    if (texturesCube[idx] !is null) {
      glBindTexture(GL_TEXTURE_CUBE_MAP, texturesCube[idx].tid);
    } else {
      int bufidx = bufmap[idx];
      if (bufidx < 0) bufidx = idx;
      glBindTexture(GL_TEXTURE_2D, textures[bufidx].tid);
    }
  }
  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_2D, texMain.tid);

  // create shader
  shader = loadShader("shader", shaderSource);

  // load buffer shaders, if necessary
  foreach (int idx; 0..4) {
    int bufidx = bufmap[idx];
    if (bufidx < 0) continue;
    if (shaderbufs[bufidx] is null) {
      // load shader
      import std.file : readText;
      import std.string : format;
      import std.path : dirName, setExtension;
      string fname = shaderFile.setExtension("")~"_buf%c.frag".format(cast(char)('a'+bufidx));
      { import std.stdio; writeln("reading buffer shader: '", fname, "'"); }
      shaderbufs[bufidx] = loadShader("buf%c".format(cast(char)('a'+bufidx)), readText(fname));
      // create fbo
      buffbos[bufidx] = new FBO(textures[bufidx]);
    }
  }

  // setup matrices
  /*
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, vlWidth, vlHeight, 0, -1, 1);
  */
  orthoCamera(vlWidth, vlHeight);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // create display list for quad
  {
    enum x0 = 0;
    enum y0 = 0;
    enum x1 = vlWidth;
    enum y1 = vlHeight;
    listQuad = glGenLists(1);
    glNewList(listQuad, GL_COMPILE);
      glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f); glVertex2i(x0, y0); // top-left
        glTexCoord2f(1.0f, 0.0f); glVertex2i(x1, y0); // top-right
        glTexCoord2f(1.0f, 1.0f); glVertex2i(x1, y1); // bottom-right
        glTexCoord2f(0.0f, 1.0f); glVertex2i(x0, y1); // bottom-left
      glEnd();
    glEndList();
  }
  //glDeleteLists(index, 1);
}


// ////////////////////////////////////////////////////////////////////////// //
//__gshared int prevmouseX = vlWidth/2, prevmouseY = vlHeight/2;
__gshared int mouseX = vlWidth/2, mouseY = vlHeight/2;
__gshared bool mBut0 = false, mBut1 = false;
__gshared float globalTime = 0.0f, prevGlobalTime = 0.0f;
__gshared float globalFrame = 0.0f;


void main (string[] args) {
  if (args.length < 2) assert(0, "filename?");
  {
    import std.algorithm : startsWith, endsWith;
    import std.file : readText, thisExePath;
    import std.path : dirName;
    version(rdmd) {
      basePath = "/home/ketmar/DUMMY-FUCK-MC/glsl_raymarching/d_glsl";
    } else {
      basePath = thisExePath.dirName;
    }
    shaderFile = args[1];
    if (shaderFile.startsWith("shaders/")) shaderFile = shaderFile[8..$];
    if (shaderFile.endsWith(".frag")) shaderFile = shaderFile[0..$-5];
    shaderFile = basePath~"/shaders/"~shaderFile~".frag";
    shaderSource = readText(shaderFile);
  }

  MonoTime lasttime = MonoTime.currTime;
  bool paused = false;

  sdwindow = new SimpleWindow(vlEffectiveWidth, vlEffectiveHeight, "OpenGL 2d soft shadows", OpenGlOptions.yes, Resizablity.fixedSize);

  sdwindow.visibleForTheFirstTime = delegate () {
    sdwindow.setAsCurrentOpenGlContext(); // make this window active
    sdwindow.vsync = false;
    //sdwindow.useGLFinish = false;
    initOpenGL();
    sdwindow.redrawOpenGlScene();
  };

  static void setShaderArgs (Shader shader) {
    //shader["iMouse"] = SVec4F(mouseX, vlHeight-1-mouseY, cast(float)mouseX/cast(float)(vlWidth-1), cast(float)(vlHeight-1-mouseY)/cast(float)(vlHeight-1));
    if (mBut0) {
      shader["iMouse"] = SVec4F(mouseX, vlHeight-1-mouseY, mouseX, vlHeight-1-mouseY);
    } else {
      shader["iMouse"] = SVec4F(0.0f, 0.0f, 0.0f, 0.0f);
    }
    //prevmouseX = mouseX;
    //prevmouseY = (vlHeight-1-mouseY);
    shader["iGlobalTime"] = globalTime;
    shader["iChannelTime"] = SVec4F(globalTime, globalTime, globalTime, globalTime);
    shader["iGlobalFrame"] = globalFrame;
    shader["iFrame"] = globalFrame;
    shader["iTimeDelta"] = globalTime-prevGlobalTime;
    auto vid = shader.varId("iDate");
    if (vid >= 0) {
      import std.datetime;
      auto now = Clock.currTime;
      glUniform4f(vid,
        now.year,
        now.month,
        now.day,
        cast(float)now.hour*3600.0f+cast(float)now.minute*60.0f+cast(float)now.second+cast(float)now.fracSecs.total!"msecs"/1000.0f
      );
    }
  }

  sdwindow.redrawOpenGlScene = delegate () {
    // first update buffers, if any
    if (!paused) {
      foreach (int idx; 0..4) {
        if (auto sd = shaderbufs[idx]) {
          buffbos[idx].exec({
            sd.exec({
              setShaderArgs(sd);
              glCallList(listQuad);
            });
          });
        }
      }
    }

    shader.exec({
      setShaderArgs(shader);
      glCallList(listQuad);
    });
  };

  enum MSecsPerFrame = 1000/60; /* FPS */

  sdwindow.eventLoop(MSecsPerFrame,
    delegate () {
      if (sdwindow.closed) return;

      if (!paused) {
        prevGlobalTime = globalTime;
        auto time = MonoTime.currTime;
        globalTime += cast(float)(time-lasttime).total!"msecs"/1000.0f;
        lasttime = time;
        globalFrame += 1.0;
      }

      sdwindow.redrawOpenGlSceneNow();
    },
    delegate (KeyEvent event) {
      if (sdwindow.closed) return;
      if (event.pressed && event.key == Key.Escape) {
        flushGui();
        sdwindow.close();
      }
    },
    delegate (MouseEvent event) {
      mouseX = event.x/scale;
      mouseY = event.y/scale;
      if (event.type == MouseEventType.buttonPressed) {
        if (event.button == MouseButton.left) mBut0 = true;
        if (event.button == MouseButton.right) mBut1 = true;
      } else if (event.type == MouseEventType.buttonReleased) {
        if (event.button == MouseButton.left) mBut0 = false;
        if (event.button == MouseButton.right) mBut1 = false;
      }
    },
    delegate (dchar ch) {
      if (ch == 'q') { flushGui(); sdwindow.close(); }
      if (ch == '0') { mouseX = mouseY = 0; }
      if (ch == ' ') {
        paused = !paused;
        if (!paused) {
          lasttime = MonoTime.currTime;
        }
      }
    },
  );
  if (!sdwindow.closed) { flushGui(); sdwindow.close(); }
  flushGui();
}
