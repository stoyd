// main driver
module xmain_mt/* is aliced*/;

private:
import core.atomic;
import core.thread;
import core.time;

import glbinds;
import glutils;


// ////////////////////////////////////////////////////////////////////////// //
//version = use_vsync;


// ////////////////////////////////////////////////////////////////////////// //
enum WindowTitle = "ShaderToy emulator";


// ////////////////////////////////////////////////////////////////////////// //
//#extension GL_ARB_compatibility : enable
enum ShaderPre = q{
#version 120
uniform vec3 iResolution;            // viewport resolution (in pixels)
uniform vec4 iMouse;                 // mouse pixel coords
uniform float iGlobalTime;           // shader playback time (in seconds)
uniform float iGlobalFrame;          // ???
uniform float iTimeDelta;            // how long last frame took to render, in seconds (TODO)
uniform float iFrame;                // buffer shaders seems to have this
uniform vec3 iChannelResolution[4];  // channel resolution (in pixels)
uniform float iChannelTime[4];       // channel playback time (in sec)
uniform vec4 iDate;                  // (year, month, day, time in seconds)
//uniform float iGlobalDelta;          // i don't know
//uniform float iSampleRate;           // sound sample rate (i.e., 44100)
};

enum ShaderMain = q{
void main (void) {
  vec2 fc = vec2(gl_FragCoord.x, gl_FragCoord.y);
  mainImage(gl_FragColor, fc);
  gl_FragColor.w = 1.0;
}
};

// ////////////////////////////////////////////////////////////////////////// //
import arsd.color;
import arsd.png;


__gshared SimpleWindow sdwindow;


public enum vlWidth = 800;
public enum vlHeight = 500;
public enum scale = 1;

public enum vlEffectiveWidth = vlWidth*scale;
public enum vlEffectiveHeight = vlHeight*scale;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string basePath;
__gshared string shaderFile;
__gshared string shaderSource;
__gshared Texture[4] textures; // 4 texture samplers
__gshared TextureCube[4] texturesCube; // 4 cube texture samplers (can be null)
__gshared Texture texMain;
__gshared Shader shader;
__gshared Shader[4] shaderbufs; // a,b,c,d
__gshared int[4] bufmap = -1; // bufmap[texnum] == bufN (or -1)
__gshared FBO[4] buffbos;
__gshared GLuint listQuad;


// ////////////////////////////////////////////////////////////////////////// //
void initOpenGL () {
  static auto loadShader (string sname, string src) {
    string spre = ShaderPre~"\n";
    // setup texture samplers
    foreach (int idx; 0..4) {
      import std.string : format;
      if (texturesCube[idx] !is null) {
        spre ~= "uniform samplerCube iChannel%s;\n".format(idx);
      } else {
        spre ~= "uniform sampler2D iChannel%s;\n".format(idx);
      }
    }
    spre ~= "#line 0\n";
    auto shader = new Shader(sname, spre~src~"\n"~ShaderMain);
    shader.exec({
      shader["iResolution"] = SVec3F(vlWidth, vlHeight, 0.0f);
      foreach (int idx; 0..4) {
        char[9] vname = "iChannel0";
        vname[$-1] = cast(char)('0'+idx);
        int bufidx = bufmap[idx];
        if (bufidx < 0) bufidx = idx;
        shader[vname[]] = cast(int)bufidx;
      }
      auto cri = shader.varId("iChannelResolution");
      float[3][4] cres = void;
      foreach (int idx; 0..4) {
        if (texturesCube[idx]) {
          cres[idx][0] = texturesCube[idx].width;
          cres[idx][1] = texturesCube[idx].height;
        } else {
          int bufidx = bufmap[idx];
          if (bufidx < 0) bufidx = idx;
          cres[idx][0] = textures[bufidx].width;
          cres[idx][1] = textures[bufidx].height;
        }
        cres[idx][2] = 0;
      }
      glUniform3fv(cri, 4, &cres[0][0]);
    });
    return shader;
  }

  glEnable(GL_TEXTURE_2D);
  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);

  // load textures
  try {
    import std.path;
    import std.stdio : File;
    int num = 0;
    foreach (/*immutable*/ line; File(shaderFile.setExtension(".tex")).byLine) {
      Texture tex;
      bufmap[num] = -1;
      if (line == "bufa" || line == "bufb" || line == "bufc" || line == "bufd") {
        // render buffer
        int bufidx = line[3]-'a';
        bufmap[num] = bufidx;
        tex = new Texture(vlWidth, vlHeight, true, Texture.Option.fp, Texture.Option.nearest, Texture.Option.clamp); // it's floating point, i guess
      } else if (line.length > 4 && line[0..4] == "cube") {
        // load cubemap texture
        import std.string : format;
        try {
          string fn = basePath~"/textures/cube/%s_%%s.png".format(line);
          auto texc = new TextureCube(fn);
          texturesCube[num++] = texc;
          if (num >= textures.length) break;
          continue;
        } catch (Exception e) {
          import std.stdio;
          writeln("can't load cube texture '", line, "'");
          assert(0);
          //tex = null;
        }
      } else {
        import std.string : format;
        try {
          string fn = basePath~"/textures/tex%s.png".format(line);
          tex = new Texture(fn);
        } catch (Exception e) {
          import std.stdio;
          writeln("can't load texture '", line, "'");
          tex = null;
        }
      }
      textures[num++] = tex;
      if (num >= textures.length) break;
    }
  } catch (Exception e) {}
  //foreach (immutable num; 0..4) if (textures[num] is null) textures[num] = new Texture(512, 512);
  foreach (immutable num; 0..4) {
    if (textures[num] is null && texturesCube[num] is null) {
      //textures[num] = new Texture(basePath~"/textures/tex00.png");
      textures[num] = new Texture(vlWidth, vlHeight, true/*, Texture.Option.fp*/);
    }
  }
  texMain = new Texture(vlWidth, vlHeight, true);

  foreach (int idx; 0..4) {
    glActiveTexture(GL_TEXTURE0+idx);
    if (texturesCube[idx] !is null) {
      glBindTexture(GL_TEXTURE_CUBE_MAP, texturesCube[idx].tid);
    } else {
      int bufidx = bufmap[idx];
      if (bufidx < 0) bufidx = idx;
      glBindTexture(GL_TEXTURE_2D, textures[bufidx].tid);
    }
  }
  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_2D, texMain.tid);

  // create shader
  shader = loadShader("shader", shaderSource);

  // load buffer shaders, if necessary
  foreach (int idx; 0..4) {
    int bufidx = bufmap[idx];
    if (bufidx < 0) continue;
    if (shaderbufs[bufidx] is null) {
      // load shader
      import std.file : readText;
      import std.string : format;
      import std.path : dirName, setExtension;
      string fname = shaderFile.setExtension("")~"_buf%c.frag".format(cast(char)('a'+bufidx));
      { import std.stdio; writeln("reading buffer shader: '", fname, "'"); }
      shaderbufs[bufidx] = loadShader("buf%c".format(cast(char)('a'+bufidx)), readText(fname));
      // create fbo
      buffbos[bufidx] = new FBO(textures[bufidx]);
    }
  }

  // setup matrices
  /*
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, vlWidth, vlHeight, 0, -1, 1);
  */
  orthoCamera(vlWidth, vlHeight);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // create display list for quad
  {
    enum x0 = 0;
    enum y0 = 0;
    enum x1 = vlWidth;
    enum y1 = vlHeight;
    listQuad = glGenLists(1);
    glNewList(listQuad, GL_COMPILE);
      glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f); glVertex2i(x0, y0); // top-left
        glTexCoord2f(1.0f, 0.0f); glVertex2i(x1, y0); // top-right
        glTexCoord2f(1.0f, 1.0f); glVertex2i(x1, y1); // bottom-right
        glTexCoord2f(0.0f, 1.0f); glVertex2i(x0, y1); // bottom-left
      glEnd();
    glEndList();
  }
  //glDeleteLists(index, 1);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int mouseX = 0, mouseY = vlHeight-1;
__gshared bool mBut0 = false, mBut1 = false;
__gshared float globalTime = 0.0f, frameTime = 0.0f;
__gshared float globalFrame = 0.0f;
__gshared bool paused = false;
shared int diedie = 0;


// ////////////////////////////////////////////////////////////////////////// //
void setShaderArgs (Shader shader) {
  //shader["iMouse"] = SVec4F(mouseX, vlHeight-1-mouseY, cast(float)mouseX/cast(float)(vlWidth-1), cast(float)(vlHeight-1-mouseY)/cast(float)(vlHeight-1));
  if (mBut0) {
    shader["iMouse"] = SVec4F(mouseX, vlHeight-1-mouseY, mouseX, vlHeight-1-mouseY);
  } else {
    shader["iMouse"] = SVec4F(mouseX, vlHeight-1-mouseY, 0.0f, 0.0f);
  }
  //prevmouseX = mouseX;
  //prevmouseY = (vlHeight-1-mouseY);
  shader["iGlobalTime"] = globalTime;
  shader["iChannelTime"] = SVec4F(globalTime, globalTime, globalTime, globalTime);
  shader["iGlobalFrame"] = globalFrame;
  shader["iFrame"] = globalFrame;
  shader["iTimeDelta"] = frameTime;
  auto vid = shader.varId("iDate");
  if (vid >= 0) {
    import std.datetime;
    auto now = Clock.currTime;
    glUniform4f(vid,
      now.year,
      now.month,
      now.day,
      cast(float)now.hour*3600.0f+cast(float)now.minute*60.0f+cast(float)now.second+cast(float)now.fracSecs.total!"msecs"/1000.0f
    );
  }
}


void renderFrame (bool paused) {
  // first update buffers, if any
  if (!paused) {
    foreach (int idx; 0..4) {
      if (auto sd = shaderbufs[idx]) {
        buffbos[idx].exec({
          sd.exec({
            setShaderArgs(sd);
            glCallList(listQuad);
          });
        });
      }
    }
  }
  shader.exec({
    setShaderArgs(shader);
    glCallList(listQuad);
  });
}


// ////////////////////////////////////////////////////////////////////////// //
void renderThread () {
  bool oldpaused = paused;
  MonoTime prevFrameStartTime = MonoTime.currTime;
  version(use_vsync) {} else MonoTime ltt = MonoTime.currTime;
  MonoTime lasttime = MonoTime.currTime;
  MonoTime time;
  enum MaxFPSFrames = 16;
  float frtimes = 0.0f;
  int framenum = 0;
  int prevFPS = -1;
  bool first = true;
  for (;;) {
    if (sdwindow.closed) break;
    if (atomicLoad(diedie) > 0) break;

    time = MonoTime.currTime;
    version(use_vsync) {
    } else {
      // max 60 FPS; capped by vsync
      //{ import core.stdc.stdio; printf("  spent only %d msecs\n", cast(int)((time-ltt).total!"msecs")); }
      if (!first && (time-ltt).total!"msecs" < 16) {
        //{ import core.stdc.stdio; printf("  spent only %d msecs\n", cast(int)((time-ltt).total!"msecs")); }
        import core.sys.posix.signal : timespec;
        import core.sys.posix.time : nanosleep;
        timespec ts = void;
        ts.tv_sec = 0;
        ts.tv_nsec = (16-cast(int)((time-ltt).total!"msecs"))*1000*1000; // milli to nano
        nanosleep(&ts, null); // idc how much time was passed
        time = MonoTime.currTime;
      }
      ltt = time;
      first = false;
    }

    auto pau = paused;
    if (oldpaused != pau) {
      lasttime = time;
      prevFrameStartTime = time;
      oldpaused = pau;
    }
    if (!pau) {
      globalTime += cast(float)(time-lasttime).total!"msecs"/1000.0f;
      lasttime = time;
      globalFrame += 1.0;
      debug { import core.stdc.stdio; printf("globalTime=%f\n", globalTime); }

      frameTime = cast(float)(time-prevFrameStartTime).total!"msecs"/1000.0f;
      prevFrameStartTime = time;

      //{ import core.stdc.stdio; printf("frametime: %f\n", frameTime*1000.0f); }
      //{ import core.stdc.stdio; printf("FPS: %d\n", cast(int)(1.0f/frameTime)); }
      frtimes += frameTime;
      if (++framenum >= MaxFPSFrames || frtimes >= 3.0f) {
        import std.string : format;
        int newFPS = cast(int)(cast(float)MaxFPSFrames/frtimes+0.5);
        if (newFPS != prevFPS) {
          sdwindow.title = "%s / FPS:%s".format(WindowTitle, newFPS);
          prevFPS = newFPS;
        }
        framenum = 0;
        frtimes = 0.0f;
      }

      //debug { import core.stdc.stdio; printf("XLockDisplay()\n"); }
      //XLockDisplay(sdwindow.display);
      debug { import core.stdc.stdio; printf("glXMakeCurrent()\n"); }
      if (glXMakeCurrent(sdwindow.display, sdwindow.window, sdwindow.glc) == 0) {
        { import core.stdc.stdio; printf("  FUUUU\n"); }
      }
      debug { import core.stdc.stdio; printf("renderFrame()\n"); }
      renderFrame(pau);
      debug { import core.stdc.stdio; printf("glXSwapBuffers()\n"); }
      glXSwapBuffers(sdwindow.display, sdwindow.window);
      debug { import core.stdc.stdio; printf("glFinish()\n"); }
      glFinish();
      // release context
      debug { import core.stdc.stdio; printf("glXMakeCurrent(0)\n"); }
      glXMakeCurrent(sdwindow.display, 0, null);
      //debug { import core.stdc.stdio; printf("XUnlockDisplay()\n"); }
      //XUnlockDisplay(sdwindow.display);
    }
  }
  atomicStore(diedie, 2);
}


// ////////////////////////////////////////////////////////////////////////// //
void closeWindow () {
  if (atomicLoad(diedie) != 2) {
    atomicStore(diedie, 1);
    while (atomicLoad(diedie) != 2) {}
  }
  if (!sdwindow.closed) {
    flushGui();
    sdwindow.close();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  if (args.length < 2) assert(0, "filename?");
  if (XInitThreads() == 0) assert(0, "XMT fucked");

  {
    import std.algorithm : startsWith, endsWith;
    import std.file : readText, thisExePath;
    import std.path : dirName;
    version(rdmd) {
      basePath = "/home/ketmar/DUMMY-FUCK-MC/glsl_raymarching/d_glsl";
    } else {
      basePath = thisExePath.dirName;
    }
    shaderFile = args[1];
    if (shaderFile.endsWith(".frag")) shaderFile = shaderFile[0..$-5];
    if (shaderFile.length && shaderFile[0] != '.') {
      if (shaderFile.startsWith("shaders/")) shaderFile = shaderFile[8..$];
      shaderFile = basePath~"/shaders/"~shaderFile~".frag";
    }
    shaderFile = shaderFile~".frag";
    shaderSource = readText(shaderFile);
  }

  Thread renderTid;

  sdwindow = new SimpleWindow(vlEffectiveWidth, vlEffectiveHeight, WindowTitle, OpenGlOptions.yes, Resizablity.fixedSize);

  sdwindow.visibleForTheFirstTime = delegate () {
    sdwindow.setAsCurrentOpenGlContext(); // make this window active
    version(use_vsync) {
      sdwindow.vsync = true;
    } else {
      sdwindow.vsync = false;
    }
    //sdwindow.useGLFinish = false;
    initOpenGL();
    /*
    //sdwindow.redrawOpenGlScene();
    renderFrame(paused);
    sdwindow.swapOpenGlBuffers();
    glFinish();
    glFlush();
    // release context
    if (glXMakeCurrent(sdwindow.display, 0, null) == 0) { import core.stdc.stdio; printf("can't release OpenGL context(0)\n"); }
    glFinish();
    glFlush();
    */
  };

  sdwindow.eventLoop(100,
    delegate () {
      if (sdwindow.closed) return;
      if (!renderTid) {
        if (glXMakeCurrent(sdwindow.display, 0, null) == 0) { import core.stdc.stdio; printf("can't release OpenGL context(1)\n"); }
        renderTid = new Thread(&renderThread);
        renderTid.start();
      }
      //sdwindow.redrawOpenGlSceneNow();
    },
    delegate (KeyEvent event) {
      if (sdwindow.closed) return;
      if (event.pressed && event.key == Key.Escape) closeWindow();
    },
    delegate (MouseEvent event) {
      if (sdwindow.closed) return;
      mouseX = event.x/scale;
      mouseY = event.y/scale;
      if (event.type == MouseEventType.buttonPressed) {
        if (event.button == MouseButton.left) mBut0 = true;
        if (event.button == MouseButton.right) mBut1 = true;
      } else if (event.type == MouseEventType.buttonReleased) {
        if (event.button == MouseButton.left) mBut0 = false;
        if (event.button == MouseButton.right) mBut1 = false;
      }
    },
    delegate (dchar ch) {
      if (ch == 'q') closeWindow();
      if (ch == '0') { mouseX = mouseY = 0; }
      if (ch == ' ') { paused = !paused; }
    },
  );
  closeWindow();
  flushGui();
}
