// z-buffer, persp-correct texturemap

vec4 inTriangle(vec2 p0, vec2 p1, vec2 p2, vec2 p)
{
  float a = 0.5*(-p1.y*p2.x + p0.y*(-p1.x + p2.x) + p0.x*(p1.y - p2.y) + p1.x*p2.y);
  float s = 1.0/(2.0*a)*(p0.y*p2.x - p0.x*p2.y + (p2.y - p0.y)*p.x + (p0.x - p2.x)*p.y);
  float t = 1.0/(2.0*a)*(p0.x*p1.y - p0.y*p1.x + (p0.y - p1.y)*p.x + (p1.x - p0.x)*p.y);

  if (s > 0.0 && t > 0.0 && 1.0 - s - t > 0.0) {
    return vec4(1.0,s,t,1.0-s-t);
  } else {
    return vec4(0.0,s,t,1.0-s-t);
  }
}

void triangle(inout vec4 c, vec2 p, int type, sampler2D tex, vec3 p0, vec3 p1, vec3 p2, vec2 t0, vec2 t1, vec2 t2)
{
  float rx = iGlobalTime;
  float ry = iGlobalTime;
  float rz = iGlobalTime;

  float cx = cos(rx); float sx = sin(rx);
  float cy = cos(ry); float sy = sin(ry);
  float cz = cos(rz); float sz = sin(rz);

  mat4 transform1 =
    mat4(1, 0, 0, 0,
           0, 1, 0, 0,
           0, 0, 1, 0,
       0, 0, -2.5, 1);

  mat4 transform2 =
    mat4(cz*cy, -sz*cy, sy, 0,
       sz*cx + cz*sy*sx, cz*cx - sz*sy*sx, -cy*sx, 0,
       sz*sx - cz*sy*cx,cz*sx + sz*sy*cx, cy*cx, 0,
       0, 0, 0, 1);

  float n = 1.0;
  float f = 10.0;
  float r = 1.0 * iResolution.x / iResolution.y;
  float t = 1.0;
  mat4 projection =
    mat4(n/r, 0, 0, 0,
           0, n/t, 0, 0,
           0, 0, -(f+n)/(f-n), -1,
       0, 0, -(2.0*f*n)/(f-n), 0);

  vec4 pt0 = vec4(0,0,0,0);
  vec4 pt1 = vec4(0,0,0,0);
  vec4 pt2 = vec4(0,0,0,0);

  pt0 = projection * transform1 * transform2 * vec4(p0,1);
  pt1 = projection * transform1 * transform2 * vec4(p1,1);
  pt2 = projection * transform1 * transform2 * vec4(p2,1);


  vec4 test = inTriangle(pt0.xy / pt0.w, pt1.xy / pt1.w, pt2.xy / pt2.w, p);

  if (test.x != 0.0) {
    float z = ((pt1.z * test.y) / pt1.w +
           (pt2.z * test.z) / pt2.w +
           (pt0.z * test.w) / pt0.w) /
          (test.y / pt1.w +
           test.z / pt2.w +
           test.w / pt0.w);
    if (z < c.w) {
      float tx = ((t1.x * test.y) / pt1.w +
            (t2.x * test.z) / pt2.w +
            (t0.x * test.w) / pt0.w) /
             (test.y / pt1.w +
            test.z / pt2.w +
            test.w / pt0.w);

      float ty = ((t1.y * test.y) / pt1.w +
            (t2.y * test.z) / pt2.w +
            (t0.y * test.w) / pt0.w) /
             (test.y / pt1.w +
            test.z / pt2.w +
            test.w / pt0.w);

      c = vec4(texture2D(tex, vec2(tx,ty)).xyz, z);
    }
  }
}

vec4 pixel(vec2 p)
{
  vec4 color = vec4(1,1,1,1000);
  // Front
  triangle(color, p, 0, iChannel0, vec3(-1,-1,-1),  vec3(1,-1,-1), vec3(-1,1,-1),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 0, iChannel0, vec3(1,-1,-1),   vec3(1,1,-1),  vec3(-1,1,-1),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));

  // Back
  triangle(color, p, 0, iChannel0, vec3(-1,-1,1),   vec3(1,-1,1),  vec3(-1,1,1),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 0, iChannel0, vec3(1,-1,1),    vec3(1,1,1),   vec3(-1,1,1),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));


  // Right
  triangle(color, p, 0, iChannel1, vec3(1,-1,-1),   vec3(1,-1,1),  vec3(1,1,-1),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 0, iChannel1, vec3(1,-1,1),    vec3(1,1,1),   vec3(1,1,-1),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));


  // Left
  triangle(color, p, 0, iChannel1, vec3(-1,-1,-1),  vec3(-1,-1,1), vec3(-1,1,-1),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 0, iChannel1, vec3(-1,-1,1),   vec3(-1,1,1),  vec3(-1,1,-1),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));

  // Bottom
  triangle(color, p, 0, iChannel2, vec3(-1,1,-1),   vec3(-1,1,1),  vec3(1,1,-1),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 0, iChannel2, vec3(-1,1,1),    vec3(1,1,1),   vec3(1,1,-1),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));

  // Top
  triangle(color, p, 0, iChannel2, vec3(-1,-1,-1),  vec3(-1,-1,1), vec3(1,-1,-1),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 0, iChannel2, vec3(-1,-1,1),   vec3(1,-1,1),  vec3(1,-1,-1),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));


  // Thru-left-right
  triangle(color, p, 0, iChannel2, vec3(-1.5,-0.5,0),  vec3(-1.5,0.5,0), vec3(1.5,-0.5,0),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 0, iChannel2, vec3(-1.5,0.5,0),   vec3(1.5,0.5,0),  vec3(1.5,-0.5,0),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));

  triangle(color, p, 0, iChannel0, vec3(-1.5,0,-0.5),  vec3(-1.5,0,0.5), vec3(1.5,0,-0.5),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 0, iChannel0, vec3(-1.5,0,0.5),   vec3(1.5,0,0.5),  vec3(1.5,0,-0.5),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));


  // In the middle
  triangle(color, p, 1, iChannel3, vec3(0,-1.5,-1.5),  vec3(0,-1.5,1.5), vec3(0,1.5,-1.5),
                        vec2(0,0),     vec2(1,0),   vec2(0,1));

  triangle(color, p, 1, iChannel3, vec3(0,-1.5,1.5),   vec3(0,1.5,1.5),  vec3(0,1.5,-1.5),
                        vec2(1,0),     vec2(1,1),   vec2(0,1));


  return color;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
  vec2 uv = fragCoord.xy / iResolution.xy;
  uv = uv * 2.0 - vec2(1.0,1.0);
  fragColor = pixel(uv);
}
