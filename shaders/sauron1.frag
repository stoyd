// noise from iq's hell shader
float noise( in vec3 x )
{
    vec3 p = floor(x);
    vec3 f = fract(x);
  f = f*f*(3.0-2.0*f);

  vec2 uv = (p.xy+vec2(37.0,17.0)*p.z) + f.xy;
  vec2 rg = texture2D( iChannel0, (uv+ 0.5)/256.0, -100.0 ).yx;
  return mix( rg.x, rg.y, f.z ) - 0.5;
}

////////////////// distance field functions from http://iquilezles.org/www/articles/distfunctions/distfunctions.htm
float sdSphere( vec3 p, float s )
{
  return length(p)-s;
}


/////////////////

// Utility stuff
#define PI 3.14159

mat3 rotx(float a) { mat3 rot; rot[0] = vec3(1.0, 0.0, 0.0); rot[1] = vec3(0.0, cos(a), -sin(a)); rot[2] = vec3(0.0, sin(a), cos(a)); return rot; }
mat3 roty(float a) { mat3 rot; rot[0] = vec3(cos(a), 0.0, sin(a)); rot[1] = vec3(0.0, 1.0, 0.0); rot[2] = vec3(-sin(a), 0.0, cos(a)); return rot; }
mat3 rotation;

const float CORONA = 0.1;
const float PUPIL = 0.2;

#define FIRE_RADIUS 0.04

vec2 map(vec3 p)
{
    // Pupil
    vec3 pp = p;
    pp *= rotation;
    pp.x *= 2.0;
    pp.y *= 0.67;
    pp -= vec3(0.0, 0.0, -0.115);

    // Coronas
    p *= rotation;

    float x = 0.5 * (atan(p.y , p.x )) / PI;
    float y = 0.5 * (atan(p.y , p.z )) / PI;

    float d = sdSphere(p, 0.15) * 0.3;
    float l = length(p);

    vec3 np = vec3(x, y, -length(p) * 0.5 + iGlobalTime * 0.03);
    float corona = abs(noise(np * 25.0));

    float corona1 = abs(noise(np * 50.0));
    float corona2 = abs(noise(np * 75.0));
    float corona3 = abs(noise(np * 200.0));
    corona += corona1 * 0.5 + corona2 * 0.35 + corona3 * 0.35;
  corona  *= FIRE_RADIUS;


    float cor = -corona + d;
    float ppl = sdSphere(pp, 0.08);


    float m = min(ppl, cor);

    if ( m == cor)
    {
        return vec2(CORONA, m);
    }
    return vec2(PUPIL, m);

}


bool trace(in vec3 from, in vec3 rd, inout vec4 color)
{
    vec3 rp = from;
    float d = 0.0;
    color.a = 0.0;
    bool hit = false;

    vec3 col = vec3(0.07, 0.020, 0.0115) * 0.28;
    float dens = 1.0;

    for (int i = 0; i < 250; ++i)
    {

        if(rp.z > 0.1)
        {
            return hit;
        }

        vec2 mat = map(rp);
        d = mat.y;
        if ( d <= 0.0 && color.a <= 1.0)
        {
            hit = true;
            float density = abs(d) * dens;
            color.a += abs(density);
            if (mat.x == CORONA)
            {
              color.rgb += col;
            }
            else if (mat.x == PUPIL)
            {
                color += vec4(0.0, 0.0, 0.0, 0.2);
            }
        }
        rp += rd * max(d * 0.2, 0.0005);
    }

    return hit;
}


void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    fragColor = vec4(0.0);
    float aspect = iResolution.x / iResolution.y;
    vec2 uv = fragCoord.xy / iResolution.xy;
    vec2 _uv = uv;

    uv -= vec2(0.5);
    vec3 rd = vec3(uv, 1.0);
    rd.y /= aspect;
    rd = normalize(rd);

    vec2 mouse = iMouse.xy / iResolution.xy;
    mouse.xy -= vec2(0.5);
    float rspeed = 20.0;

    if (iMouse.xy == vec2(0.0))
    {
      mouse.xy = vec2(0.0, 0.0);
    }
    float rotspeed = 4.0;
    mouse.x += sin(iGlobalTime * 0.5 * rotspeed) * 0.015;
    mouse.y += sin(iGlobalTime * 0.4 * rotspeed) * 0.015;

    rotation = roty(-mouse.x * rspeed);
    rotation *= rotx(mouse.y * rspeed);

    trace(vec3(0.0, 0.0, -1.0), rd, fragColor);
    fragColor.a = clamp(fragColor.a, 0.0, 1.0);
    uv.y *= iResolution.y / iResolution.x;

    // Background texture
    vec4 bg = vec4(0.0);
    for (int i = 0; i < 3; ++i)
    {
        float spd = float( (i + 1)) * iGlobalTime * 0.05;
        vec2 c1 = vec2(uv.x, pow(_uv.y, 0.25) - spd);
        c1.x *= pow(length(uv), 2.8) * 5.5;

        if ( i == 0)
        {
          vec4 smoke = (1.0 - fragColor.a) * texture2D(iChannel2, c1 * 6.0);
          bg += smoke * float(i + 1) * 0.45 * vec4(0.5, 0.5, 0.3, 0.0);
        }else
        {
          vec4 smoke = (1.0 - fragColor.a) * texture2D(iChannel1, c1);
          bg += smoke * float(i + 1) * 0.25 * vec4(0.9, 0.3, 0.1, 0.0);
        }
    }

    // look colors
    vec3 look = rotation * vec3(0.0, 0.0, -1.0);
    float lookd = clamp(dot(look, vec3(0.0, 0.0, -1.0)), 0.0, 1.0);
    fragColor += mix(bg, bg * vec4(0.2, 1.3, 3.5, 0.0), mix(1.0 - lookd, 1.0, 0.00));

    //
    vec4 glow = vec4((sin(iGlobalTime * 2.0) * 0.5) * 1.5 * vec4(0.8, 0.25, 0.1, 0.0));
    fragColor += glow * (0.5 - length(uv));

    fragColor -=  clamp(length(uv) - 0.2,  0.0, 1.0) *  (1.0 - fragColor.a) * 1.2;


}
