// compact version of https://www.shadertoy.com/view/XtfSD8
// 2553-> 1105

float  t= iGlobalTime;

#define R(a) mat2(cos(a), -sin(a), sin(a), cos(a))

float fbm(vec2 p) {
  //p *= 1.1;   // subtil noise tunning not useful for ascii art
  float f = 0.,  a = 2.;
  for( int i = 0; i < 3; i++)
      f += ( sin(p.x*10.) * sin(p.y*(3. + sin(t/33.))) + .2 )/a,
      p +=p,   // p*= 2.*R(t/150. * float(i*i)),
      a +=a; // a*=2.2

  return f;
}

float digit(vec2 p) {

    p = .5 + (p-.5) * (1.+.15*pow(dot(p,p),.3))
        + sin(t/vec2(7,13))/5.;

    vec2 g = vec2(45,15), f,
         s = .1*floor(p *=g)/g,
    // fluid pattern
         q = vec2( fbm(s + 1.), fbm(R(t/30.)*s + 1.)),
         r = vec2( fbm(R(.1)*q ), fbm(q ));
    int  n = int(fbm(s + 1.*r)*13. - .3) ;

    p = fract(p)*1.2;
    if ( max(p.x,p.y)>1. ) return 0.;
    p.y = 1.-p.y;
    f = fract(p*=5.); p -= f;

    // sampleFont
    float a = n<1?  .91333008: n<2?  .27368164: n<3? 1.87768555: n<4? 1.87719727: n<5? 1.09643555:
              n<6? 1.97045898: n<7?  .97045898: n<8? 1.93945312: n<9?  .90893555:       .90893555,
          b = n<1?  .89746094: n<2?  .06933594: n<3? 1.26513672: n<4? 1.03027344: n<5? 1.51611328:
              n<6? 1.03027344: n<7? 1.27246094: n<8? 1.03222656: n<9? 1.27246094:      1.52246094,
         pos = floor(p.x + p.y * 5.);
    if (pos>12.) pos-=13., a=b;
    return step(1., mod(exp2(pos)*a, 2.))
            * (.2 + f.y*4./5.) * (.75 + f.x/4.) ;
}


void mainImage( out vec4 f, in vec2 p) {

    p /= iResolution.xy;

    // displace
    float y = p.y - fract(t/4.);
  p.x += sin(p.y*20. + t)/80.
          * step(.8, sin(t + 4.*cos(t+t)))
          * (1.+cos(t*60.))
          / (1.+50.*y*y);


    float s = .9*digit(p); f+= s;
/*      //   ultra light blur : not really visible ( -> replaced by the line above)
    float s = 0.;
    for (int i = -1; i < 2; i++)
        for (int j = -1; j < 2; j++)
            s += digit(p+vec2(i,j)/5e2)/10.;

    f+= .9*digit(p);
*/

    f.g += s*  ( fract(p.y+t*20.) < .2 ?  1.4  : 1.);
}
