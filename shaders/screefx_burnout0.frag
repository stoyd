#define r(s) fract(43.*sin((i+s).x*13.+(i+s).y*78.))

void mainImage (out vec4 o, vec2 i) {
  o -= o;
  float t = fract(0.1*iDate.w), f = 2.0;
  for (int k = 0; k < 9; ++k) {
    vec2 w = f*4.5*i/iResolution.x, i = floor(w), j = vec2(1,0);
    w -= i;
    w = w*w*(3.-w-w);
    o += mix(mix(r(0.0), r(j), w.x), mix(r(j.yx), r(1.0), w.x), w.y)/f;
    f += f;
  }
  o = smoothstep(t, t+0.1, o)*vec4(5,2,1,1);
}
