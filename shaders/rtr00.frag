// Simple (inefficient) proof-of-concept implicit-surface ray-tracer by Jakob Thomsen
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// (ported from http://sourceforge.net/projects/shaderview/)

#define pi 3.1415926

float sfract(float val, float scale)
{
    return (2.0 * fract(0.5 + 0.5 * val / scale) - 1.0) * scale;
}

float fn(vec3 v)
{
    float x = v.x;
    float y = v.y;
    float z = v.z;
    //x = sfract(x * 2.0, 1.0);
    return min(min(x * x + y * y + z * z - 0.5, x * x + y * y - 0.125), min(x * x + z * z - 0.125, y * y + z * z - 0.125));
}

float comb(float v, float s)
{
    return pow(0.5 + 0.5 * cos(v * 2.0 * pi), s);
}

vec3 tex(vec3 v)
{
    float x = v.x;
    float y = v.y;
    float z = v.z;
    //float d = exp(-pow(z * 20.0 + (2.0 * fract((0.5 + 0.5 * time) / 4.0) - 1.0) * 4.0 * 5.0, 2.0));
    float d = exp(-pow(abs(z * 20.0 + sfract(iGlobalTime, 4.0) * 5.0), 2.0));
    //x += 0.01 * time;
    //y += 0.01 * time;
    z += 0.1 * iGlobalTime;
    x = (x * 8.0);
    y = (y * 8.0);
    z = (z * 8.0);
    float q = 0.0;
    q = max(q, comb(x, 10.0));
    q = max(q, comb(y, 10.0));
    q = max(q, comb(z, 10.0));
    float w = 1.0;
    w = min(w, max(comb(x, 10.0), comb(y, 10.0)));
    w = min(w, max(comb(y, 10.0), comb(z, 10.0)));
    w = min(w, max(comb(z, 10.0), comb(x, 10.0)));
    return (w + vec3(0.0, q, 0.5 * q)) + d * 5.0;
}

vec3 camera(vec2 uv, float depth)
{
    float t = iGlobalTime * 0.1;
    vec3 v;
    v.x = uv.x * cos(t) + uv.y * sin(t); // uv.x;
    v.y = uv.x * -sin(t) + uv.y * cos(t); // uv.y;
    v.z = depth;

    // isometry
    vec3 iso;
    iso.x =  v.x - v.y - v.z;
    iso.y = -v.x - v.y - v.z;
    iso.z =        v.y - v.z;

    return iso;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float t = iGlobalTime * 0.1;
    vec2 uv = 2.0 * fragCoord.xy / iResolution.xy - 1.0;
    uv.x *= iResolution.x / iResolution.y;

    const int depth = 256;
    float m = 0.0;
    vec3 color = vec3(0.0, 0.0, 0.0);
    for(int layer = 0; layer < depth; layer++) // slow...
    {
        vec3 v = camera(uv, 2.0 * float(layer) / float(depth) - 1.0);

        if(abs(v.x) > 1.0 || abs(v.y) > 1.0 || abs(v.z) > 1.0)
            continue;

        if(abs(fn(v)) < 0.05)
        {
            m = 2.0 * float(layer) / float(depth) - 1.0;
            color = tex(v);
        }
    }

    fragColor = vec4(color * vec3(m), 1.0);
}
