//by Ethan Alexander Shulman known as public_int_i
//This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
//http://creativecommons.org/licenses/by-nc-sa/4.0/

const vec2 sunPos = vec2(.5);
const vec3 sunColor = vec3(1.,.95,.95);

const float sunSize = .06;
const float godrayReach = .3;
const int godrayIter = 32;
const float godrayIntensity = .045;
const float godrayStep = (sunSize/2.)/float(godrayIter);


const vec3 backgroundColor = vec3(0.);

const vec2 occlusionSize = vec2(.1);
vec2 occlusionLoc = iMouse.xy/iResolution.xy;


float occlusionMap(in vec2 uv) {
    float d = length(max(abs(uv-occlusionLoc)-vec2(occlusionSize),0.));
    d = max(-(length(mod(uv-occlusionLoc,occlusionSize*.5)-occlusionSize*.25)-occlusionSize.x*.5),d);
    return floor(1.03-d);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
  vec2 uv = fragCoord.xy / iResolution.xy;

    if (iMouse.w < 1.) {
        occlusionLoc = vec2(sin(iGlobalTime*.6)*.2+.5,cos(iGlobalTime*.76)*.2+.5);
    }

    float cl = occlusionMap(uv);
    vec2 sunDir = sunPos-uv;
    float sunLen = length(sunDir);
    if (sunLen < sunSize) {
        fragColor = vec4(mix(sunColor,vec3(cl*.3),cl),1.);
        return;
    }
    vec3 c = backgroundColor;

    float cb;
    c = mix(c,vec3(cl*.3),cl);
    cl = 0.;
    if (sunLen < godrayReach) {

        sunDir = normalize(sunDir);
        uv += sunDir*max(0.,(sunLen-sunSize));
        sunLen = 1.-sunLen/godrayReach;
        int maxIter = int(sunLen*float(godrayIter));

        for (int i = 0; i < godrayIter; i++) {

            cl += max(0.,1.-occlusionMap(uv))*sunLen;

            if (i > maxIter) {
                break;
            }

            uv += sunDir*godrayStep;
        }

        cl *= godrayIntensity;
        c += min(1.,cl)*sunColor;
    }

    fragColor = vec4(c,1.);
}
