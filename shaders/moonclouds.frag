float rand(vec2 n)
{
    return fract(cos(dot(n,vec2(1.4381,2.3563)))*5647.3674);
}
float noise(vec2 n)
{
    float h1 = mix(rand(vec2(floor(n.x),floor(n.y))),rand(vec2(ceil(n.x),floor(n.y))),smoothstep(0.0,1.0,fract(n.x)));
  float h2 = mix(rand(vec2(floor(n.x),ceil(n.y))),rand(vec2(ceil(n.x),ceil(n.y))),smoothstep(0.0,1.0,fract(n.x)));
  return mix(h1,h2,smoothstep(0.0,1.0,fract(n.y)));
}
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
  vec2 uv = fragCoord.xy / iResolution.x;
    vec2 p = fragCoord.xy;
    vec2 os = vec2(iGlobalTime-10.5,0.0);
    float moon = clamp((0.1-length(uv-vec2(0.4)))*512.0,0.0,1.0)*(1.0-0.5*noise(p/16.0))+(0.4-length(uv-vec2(0.4)));
    float cloud = noise(p/64.0+os)*0.3+noise(p/48.0+os)*0.4+noise(p/32.0+os)*0.2+noise(p/16.0+os)*0.1;
    cloud *= noise(p/128.0+os*0.5)*0.3+noise(p/256.0+os*0.35)*0.7;
  fragColor = vec4((moon*cloud)*vec3(1.25,1.3,1.0),1.0);
}
