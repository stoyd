// Meet Billy by eiffie
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// V2 reduced the number of bobbly unions and put his thumb joints back

#define STEPS 32.0
#define DETAIL 0.01

#define time iGlobalTime*0.75
#define size iResolution
#define tex iChannel0

vec3 L;

float RRect(in vec3 z, vec4 r){return length(max(abs(z.xyz)-r.xyz,0.0))-r.w;}
float BUn(float a, float b, float mx){a+=mx;b+=mx;return (a+b-sqrt(abs(a*a+b*b-2.0*(1.0-mx)*a*b)))/(2.0-mx)-mx;}//from knighty??
float RCyl(in vec3 z, vec3 r){return length(max(vec2(abs(z.y)-r.y,length(z.xz)-r.x),0.0))-r.z;}
float Torus(in vec3 z, vec2 r){return length(vec2(length(z.xy)-r.x,z.z))-r.y;}
float Tube(vec3 pa, vec3 ba, float r){return length(pa-ba*clamp(dot(pa,ba)/dot(ba,ba),0.0,1.0))-r;}
vec2 rotate(vec2 v, float angle) {return cos(angle)*v+sin(angle)*vec2(v.y,-v.x);}

const mat2 rm1=mat2(0.863,0.5055,-0.5055,0.863),rm2=mat2(0.66,-0.7513,0.7513,0.66);
float handSpread,handHeight;
vec3 F1,F2,F3;
const vec3 T1=vec3(-0.625,0.375,-0.25);
float DEHand(in vec3 z){//hand
  z.x=abs(z.x);z-=vec3(handSpread,handHeight,-4.0);
  vec3 p=z*(1.0+z.x*0.1)*1.5;
  float dW=RRect(p+vec3(0.0,4.0,0.0),vec4(0.15-p.y*0.075,3.0,-0.1-p.y*0.075,0.375));
  float dP=RRect(p,vec4(0.3,0.35,-0.35,0.85));
  vec3 pF=p;pF.y-=1.0;
  pF.x=abs(abs(pF.x)-0.5)-0.25;
  float dF=Tube(pF,F1,0.21);pF-=F1;
  dF=BUn(dF,Tube(pF,F2,0.189),0.1);pF-=F2;
  dF=BUn(dF,Tube(pF,F3,0.17),0.1);
  pF=p+vec3(0.75,0.5,0.0);
  float dT=Tube(pF,T1,0.21);pF-=T1;
  dT=BUn(dT,Tube(pF,vec3(-F1.y,0.375,F1.z),0.189),0.1);
  float d=min(dW,dF);
  d=BUn(d,dP,0.2);
  d=BUn(d,dT,0.4);
  return d/((1.0+z.x*0.1)*1.5);
}

const mat2 rm3=mat2(0.9394,0.343,-0.343,0.9394),rm4=mat2(0.6058,-0.7956,0.7956,0.6058);
float DE(in vec3 z){//billy
  vec3 p=z;
  float dH=length(p)-3.0;
  float dN=length(p+vec3(0.0,0.1,3.0))-0.35;
  float dM=length(p+vec3(0.0,0.76,2.32))-0.36;
  float dS=length(p+vec3(0.0,6.0,0.0))-4.0;
  p.y+=1.0;p.z+=0.78;
  p.yz=rm3*p.yz;
  float dC=RCyl(p,vec3(1.38,0.0,0.68));
  p=z;
  p.x=abs(p.x);
  float dN2=length(p+vec3(-0.4,0.25,3.0))-0.15;
  p+=vec3(-1.08,-0.28,2.5);
  float dY=length(p)-0.45;
  p.yz=rm1*p.yz;
  float dB=Torus(p,vec2(0.666,0.2));
  p+=vec3(-1.8,1.36,-1.6);
  p.xz=rm4*p.xz;
  float dE=Torus(p,vec2(0.666,0.2));
  dN=BUn(dN,dN2,0.2);
  float d=min(dN,min(dE,min(dC,dB)));
  d=BUn(dH,d,0.2);
  dM=min(dM,dS);
  d=BUn(d,dM,-0.4);
  return min(d,min(dY,DEHand(z)));
}
float spec=0.0,specExp=2.0,eyeLid;
mat3 emx;
vec3 Color(in vec3 z, float d){//color
  vec3 p1=z+vec3(0.0,0.76,2.32);
  float dM=length(p1)-0.36;
  vec3 p=z;
  p.x=abs(p.x);
  p+=vec3(-1.08,-0.28,2.5);
  float dY=length(p)-0.45;
  vec2 pt=p.xy;
  vec3 c=vec3(0.0);
  float s=1.0,mn=0.0,mx=1.0;
  if(abs(d-dY)<0.01 && z.y<eyeLid+sin(z.x*100.0)*0.02){
    if(z.y>eyeLid-0.02)p=vec3(0.75,0.5,0.2);
    else{
      p=emx*p;
      float t=length(p.xy);
      spec=max(0.0,1.0-t*3.0);
      if(t<eyeLid*0.15+0.02){p=vec3(0.0);specExp=4.0;}
      else{
        float t2=smoothstep(0.15,0.16,t);
        pt=mix(vec2(p.z*10.0,atan(p.x,p.y)*0.1),pt.yx*0.25,t2);
        p=mix(vec3(0.4,0.7,0.9),vec3(1.0),t2);
        c=mix(vec3(-0.75),vec3(0.0,-1.0,-1.0)*t*2.0,t2);
      }
    }
  }else if(abs(d-dM+0.15)<0.1){
    p=mix(vec3(0.0),vec3(1.0,0.8,0.6),smoothstep(0.08,0.1,abs(d-dM+0.15)));
    float t=length(p1.xy);
    p1.x=abs(p1.x)-0.125;
    if(abs(p1.x)<0.1 && p1.y>0.075 && t<0.28){spec=1.0;p=vec3(1.0,1.0,0.6);}

  }else if(z.y<-2.25)p=vec3(2.0);
  else {
    p=vec3(1.0,0.8,0.6);
    c=vec3(-0.25);
    pt.y*=0.1;
  }
  return p+c*clamp(texture2D(tex,pt).g*s,mn,mx);
}

float AO( vec3 p, vec3 n ) {//kali's version of iq's?? (I'm using it as very soft shadow/ao.)
  float aodet=0.27,d=aodet,totao = 0.0;
  for( int aoi=1; aoi<5; aoi++ ) {
    totao+=(d-DE(p+n*d))*exp(-1.242*d);
    d+=aodet;
  }
  return clamp(1.-totao, 0., 1.0 );
}

float light;
vec3 scene(vec3 ro, vec3 rd){
  float t=0.0,d=1.0,st;
  vec3 N,C=vec3(0.0);
  for(float i=0.0;i<STEPS;i+=1.0){
    if(t>10.0 || d<DETAIL)continue;
    t+=d=DE(ro+rd*t);
    st=i;
  }
  if(d<0.1){
    ro+=rd*t;
    C=Color(ro,d);
    const vec2 v=vec2(0.001,0.0);
    N=normalize(vec3(-DE(ro-v.xyy)+DE(ro+v.xyy),-DE(ro-v.yxy)+DE(ro+v.yxy),-DE(ro-v.yyx)+DE(ro+v.yyx)));
    C*=max(0.0,0.25+dot(L,N)*0.75); //diffuse
    C+=spec*pow(max(0.0,dot(reflect(rd,N),L)),specExp); //specular
    C*=1.0-0.5*st/STEPS; //a cheap ao
    C*=AO(ro,L); //a cheap shadow/ao
    C*=clamp(light-t*t*0.01,0.0,1.0); //attenuation and flicker
  }
  return C;
}

mat3 lookat(vec3 fw,vec3 up){
  fw=normalize(fw);vec3 rt=normalize(cross(fw,normalize(up)));return mat3(rt,cross(rt,fw),fw);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
  L=normalize(vec3(0.25,-0.5,-1.0));
  vec3 ro=vec3(-0.25,-0.5,-8.0);
  vec3 rnd=texture2D(tex,vec2(0.1,0.01)*time).rgb;
  float fingerCurl=rnd.g;
  eyeLid=rnd.b*2.0;
  ro.y+=rnd.r;
  light=rnd.r;
  rnd=texture2D(tex,vec2(0.02,0.002)*time).rgb*4.0;
  handSpread=1.5+rnd.g;
  handHeight=-rnd.b;
  ro.z+=rnd.g*2.0;
  ro.x-=rnd.b;

  F1=vec3(0.0,0.54,0.0);F2=vec3(0.0,0.459,0.0);F3=vec3(0.0,0.39,0.0);
  F1.zy=rotate(F1.zy,-fingerCurl*2.0);
  F2.zy=rotate(F2.zy,-fingerCurl*4.0);
  F3.zy=rotate(F3.zy,-fingerCurl*8.0);

  emx=lookat(vec3(2.0,2.0,6.0)-rnd,vec3(0.0,1.0,0.0));
  float tim=abs(15.0-mod(time+15.0,30.0));
  mat3 cmx=lookat(-ro+vec3(cos(tim*3.3),sin(tim*1.3),0.0)*4.0/tim,vec3(0.0,1.0,0.0));
  vec3 rd=cmx*normalize(vec3((2.0*fragCoord.xy-size.xy)/size.y,1.0));
  vec3 color=scene(ro,rd);
  fragColor = vec4(color,1.0);
}
