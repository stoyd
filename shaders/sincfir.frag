#define NUM_COEF 128
#define TAU 6.283185307179586476925286766559

float Sinc(float filterCoeffs, int i)
{
    float ret = 0.0;
  if ((i-(NUM_COEF/2)) == 0)
  {
    ret = (TAU * filterCoeffs);
  }else
  {
    ret = sin(TAU * filterCoeffs * float(i-(NUM_COEF/2))) / float(i-(NUM_COEF/2));
  }

    // Hann window...
    ret *= 0.5 * (1.0 - cos(TAU*float(i)/float(NUM_COEF)));

    // Blackman window
  //ret *= 0.54 - 0.46 * cos(TAU * float(i) / float(NUM_COEF));

    ret /= (TAU * filterCoeffs)*1.3;
    return ret;
}

float Line(vec2 a, vec2 b, vec2 p)
{
  vec2 pa = p - a;
  vec2 ba = b - a;
  float h = clamp(dot(pa,ba) / dot(ba,ba), 0.0, 1.0 );
    h = length( pa - ba*h );
    return pow(smoothstep(-0.03, .03, h) * smoothstep(0.03, -.03, h), .2)*1.3;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
  vec2 uv = fragCoord.xy / iResolution.xy;
    uv.y -=.2;
    float time = iGlobalTime;

    int i = int(uv.x * (float(NUM_COEF)-.1));
    float cutoff = 31.0 + cos(time*.7) * 30.0;
    cutoff = cutoff * cutoff;

//    if (iMouse.z > 0.0)
//    {
//        cutoff = iMouse.x / iResolution.x * 1000.0;
//    }

    vec2 p1 = vec2(float(i)   / float(NUM_COEF), Sinc(2.0 * cutoff / iSampleRate, i));
    vec2 p2 = vec2(float(i+1) / float(NUM_COEF), Sinc(2.0 * cutoff / iSampleRate, i+1));

    vec3 col = vec3(Line(p1, p2, uv));
  fragColor = vec4(col,1.0);
}
