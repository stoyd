// Spiderweb by eiffie based on knighty's spirals - https://www.shadertoy.com/view/ls2GRz
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.


#define time iGlobalTime*0.75
#define size iResolution
#define tex iChannel0

float Ellipsoid(vec3 z, vec4 r){float f=length(z*r.xyz);return f*(f-r.w)/length(z*r.xyz*r.xyz);}//from iq
float Tube(vec3 pa, vec3 ba, float r){return length(pa-ba*clamp(dot(pa,ba)/dot(ba,ba),0.0,1.0))-r;}//from iq as well :)

#define PI 3.14159265
const vec2 c=vec2(20.0,1.0);
const float tpdlc=0.3137673;//precalculated 2*PI/length(c)
//Draws two perpendicular spirals (from knighty and then I messed it up with gravity)
float dspiral(vec3 p){//original https://www.shadertoy.com/view/ls2GRz
  float r=length(p.xy);
  vec2 f=vec2(log(r),atan(p.y,p.x))*0.5/PI;
  vec2 d=f.y*vec2(c.x,-c.y)-f.x*c.yx;
  d=(0.5-abs(fract(d)-0.5))*r*tpdlc;
  float g=sqrt(abs(d.x-d.y));
  p.y+=g*0.1;//now add gravity
  p.xy*=1.0+g*0.02;//straighten a bit
  r=length(p.xy);//and recalculate the second spiral
  f=vec2(log(r),atan(p.y,p.x))*0.5/PI;
  float d1=-f.y*c.y-f.x*c.x;
  d1=(0.5-abs(fract(d1)-0.5))*r*tpdlc;
  return min(sqrt(d.x*d.x+p.z*p.z),max(sqrt(d1*d1+p.z*p.z),-r+0.6));
}

const mat2 rmx=mat2(0.995953,0.0898785,-0.0898785,0.995953);
const vec3 L1=vec3(0.8500,0.7100,0.0),L2=vec3(0.5300,0.2700,0.05),L3=vec3(0.5600,0.0800,0.05);
const vec3 SP=vec3(0.15,0.15,0.15);
float dspider(vec3 p){//a spider
  p.xyz+=SP;
  p.xy*=rmx;
  float dB=Ellipsoid(p,vec4(2.0,0.9,3.0,0.33));
  p.y-=0.32;
  dB=max(dB,-length(p.xy)+0.06);
  p.y+=0.98;
  float d=Ellipsoid(p,vec4(2.0,1.0,3.6,0.5));
  p.y-=0.66;p.z+=0.06;
  p.xy=abs(p.xy)-0.14;
  if(p.x<p.y)p.xy=p.yx;
  float dJ=length(p+vec3(0.03,0.07,0.0))-0.055;
  p.xy+=vec2(0.32);
  p.y=p.y+sin(p.x*9.8+p.y)*0.01*length(p.xy);
  float dL=Tube(p,L1,0.03);
  dL=min(dL,Tube(p-L1,L2,0.0225));
  dL=min(dL,Tube(p-L1-L2,L3,0.0075));
  return min(d,min(dB,min(dL,dJ)));
}
float DE(vec3 p)
{
  return min(dspiral(p),dspider(p));
}
vec3 clr;
float CE(vec3 p){//colors and f's with the normal
  float d;
  if(p.z<-0.01 && length(p.xy)<2.25){
    d=dspider(p)+texture2D(iChannel0,p.xy).r*0.01;
    p.xyz+=SP;
    p.xy*=rmx;
    float dB=Ellipsoid(p,vec4(2.0,0.9,3.0,0.33));
    p.y+=0.66;
    dB=min(dB,Ellipsoid(p,vec4(2.0,1.0,3.6,0.56)));
    p.xy=abs(p.xy);
    if(dB<0.01){
      clr+=vec3(vec2(0.5*abs(cos(length(p.xy)*10.0)-sin((p.x+p.y)*50.0)*0.25-0.5)),0.0);
    }else{
      p.y-=0.66;
      clr+=vec3(0.5*vec2(1.0-abs(sin(length(p.xy)*20.0))),0.0)*sin(length(p.xy)*2.0);
    }
  }else {
    clr+=vec3(1.0);d=dspiral(p);
  }
  return d;
}

mat3 lookat(vec3 fw,vec3 up){
  fw=normalize(fw);vec3 rt=normalize(cross(fw,normalize(up)));return mat3(rt,cross(rt,fw),fw);
}
float AO( vec3 p, vec3 n ) {//kali's version of iq's?? (I'm using it as very soft shadow/ao.)
  float aodet=0.47,d=aodet,totao = 0.0;
  for( int aoi=1; aoi<5; aoi++ ) {
    totao+=(d-DE(p+n*d))*exp(-1.68*d);
    d+=aodet;
  }
  return clamp(1.-totao, 0., 1.0 );
}

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
  vec3 ro=vec3(-0.5+sin(time),-1.0+sin(time*0.4),-3.0+sin(time*1.3))+texture2D(tex,vec2(0.02,0.002)*time).rgb*0.5;
  vec3 L=normalize(vec3(0.5,0.5,-0.75));
  vec3 rd=lookat(-ro,vec3(0.0,1.0,0.0))*normalize(vec3((2.0*fragCoord.xy-size.xy)/size.y,1.0));
  vec3 col=textureCube(iChannel1,vec3(-rd.x,rd.y,-rd.z)).rgb;
  vec2 trap=vec2(100.0);
  float t=-(ro.z+0.5)/rd.z,d=1.0;
  for(int i=0;i<24;i++){
    t+=d=DE(ro+rd*t)*0.8;
    if(d<trap.x)trap=vec2(d,t);
  }
  float mind=2.0*trap.y/size.y;
  if(trap.x<mind){
    ro+=rd*trap.y;
    float spec=1.0,dif=1.0,fade=0.0;
    vec2 v=vec2(mind*0.666,0.0);
    clr=vec3(0.0);
    float d=CE(ro),d1=CE(ro-v.xyy),d2=CE(ro+v.xyy);
    float d3=CE(ro-v.yxy),d4=CE(ro+v.yxy);
    float d5=CE(ro-v.yyx),d6=CE(ro+v.yyx);
    vec3 hcol=clr*0.143;
    vec3 N1=normalize(vec3(-d1+d,-d3+d,-d5+d));
    vec3 N2=normalize(vec3(-d+d2,-d+d4,-d+d6));
    if(hcol.b<0.5){
      spec=max(0.0,1.0-length(ro.xy+vec2(0.0,0.66)));
      dif*=(max(0.0,0.25+dot(N1,L)*0.75)+max(0.0,0.25+dot(N2,L)*0.75))*0.5*AO(ro,L);
    }else {
      dif=(max(0.5,0.75+dot(N1,L)*0.25)+max(0.5,0.75+dot(N2,L)*0.25))*0.5;
      fade=trap.y*0.1;
    }
    hcol*=dif;
    hcol+=0.5*spec*(pow(max(0.0,dot(reflect(rd,N1),L)),8.0)+pow(max(0.0,dot(reflect(rd,N2),L)),8.0))*vec3(1.0,0.75,0.25);
    col=mix(hcol,col,min(1.0,smoothstep(0.0,mind,max(0.0,trap.x))+fade));
  }
  fragColor = vec4(clamp(col,0.0,1.0),1.0);
}
