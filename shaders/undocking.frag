// Undocking by eiffie
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// fake AA/DOF I've seen this done before but don't remember by whom or if I'm doing it
// the same ???? This doesn't handle the object occluding itself - that comes next:)

#define STEPS 78
#define SHADOWSTEPS 8
#define DETAIL 0.005

#define tex iChannel0
#define size iResolution
#define time iGlobalTime*0.2

float RRect(in vec3 z, vec4 r){return length(max(abs(z.xyz)-r.xyz,0.0))-r.w;}
float DE(vec3 z0)
{
  vec3 z=z0;
  z.xy*=(vec2(1.0)+sin((z.xz+z.yx)*vec2(3.7,0.73))*vec2(0.005,0.1));
  z.y+=0.5;
  vec3 a=z;
  a.z=abs(a.z)-0.47;
  a.y+=0.13;
  float dS=RRect(a,vec4(9.0,0.11,0.02,0.01));
  z.x=clamp(z.x,-3.0,3.0)*2.0-z.x;
  z.x=clamp(z.x,-1.0,1.0)*2.0-z.x;
  a=z;a.y+=0.38;
  dS=min(dS,RRect(a,vec4(0.02,0.11,0.65,0.01)));
  a=z;
  a.x=clamp(z.x,-0.333,0.333)*2.0-a.x;
  a.x=abs(a.x)-0.16;
  dS=min(dS,RRect(a,vec4(0.11,0.02,0.6,0.01)));
  z.x-=0.075;
  z.z=abs(z.z)-0.6;
  z.y+=0.2;
  return min(dS,length(max(vec2(abs(z.y)-1.25,length(z.xz)-0.075),0.0))-0.01);
}

vec4 Color(vec3 z0)
{
  z0.xy*=(vec2(1.0)+sin((z0.xz+z0.yx)*vec2(3.7,0.73))*vec2(0.005,0.1));
  vec3 z=z0;
  z.y+=0.5;
  vec3 a=z;
  a.z=abs(a.z)-0.47;
  a.y+=0.13;
  float dB=RRect(a,vec4(9.0,0.11,0.02,0.01));
  z.x=clamp(z.x,-3.0,3.0)*2.0-z.x;
  z.x=clamp(z.x,-1.0,1.0)*2.0-z.x;
  a=z;a.y+=0.38;
  float dB2=RRect(a,vec4(0.02,0.11,0.65,0.01));
  a=z;
  a.x=clamp(z.x,-0.333,0.333)*2.0-a.x;
  a.x=abs(a.x)-0.16;
  float dB3=RRect(a,vec4(0.11,0.02,0.6,0.01));
  z.x-=0.075;
  z.z=abs(z.z)-0.6;
  z.y+=0.2;
  float dP=length(max(vec2(abs(z.y)-1.25,length(z.xz)-0.075),0.0))-0.01;
  vec4 col=vec4(0.25);
  float dS=min(dB,min(dB2,dB3));
  vec2 pt;
  if(dP<dS){
    float a=atan(z.z,z.x);
    pt=vec2(z.y,a*0.15);
    float sp=texture2D(tex,vec2(a,z.y)).g*2.25;
    col.a=clamp(-2.75-2.0*z.y+sp,0.0,1.0);
    col.rgb=vec3(0.5,1.0,0.8)*clamp(1.5+2.0*z.y+sp,0.0,1.0);
  }else{
    if(dS==dB)pt=z0.xy;
    else if(dS==dB2)pt=z0.zy;
    else pt=z0.zx;
    col.rgb=vec3(0.7,1.0,0.8);
  }
  col.rgb*=texture2D(tex,pt).rgb;
  return col;
}

mat2 rmx;
const vec2 v=vec2(0.001,0.0);
vec3 getWaterNormal(vec3 ro){
  ro.xz=rmx*ro.xz+vec2(-time*0.05,0.0);
  float d=texture2D(tex,ro.zx).g;
  return normalize(vec3(-d+texture2D(tex,ro.xz+v.xy).g,2.0,-d+texture2D(tex,ro.xz+v.yx).g));
}

vec3 L;
vec3 scene(vec3 ro, vec3 rd){
  vec4 col;
  const float maxDepth=20.0;
  float wt=(-1.75-ro.y)/rd.y,maxD=maxDepth;
  vec3 brd=reflect(rd,getWaterNormal(ro+rd*wt));
  vec2 trap=vec2(100.0);
  if(rd.y<0.0)maxD=min(maxD,wt);
  float t=0.0,d=1.0,ts=0.0;
  for(int i=0;i<STEPS;i++){
    if(t>maxD){
      if(maxD<maxDepth){
        if(trap.x>=DETAIL*trap.y){trap=vec2(100.0);ts=t;}
        ro+=rd*maxD;maxD=maxDepth;rd=brd;t=0.0;
      }else continue;
    }
    t+=d=DE(ro+rd*t)*0.9;
    if(d<trap.x)trap=vec2(d,t);
  }
  float MIND=DETAIL*(trap.y+ts);
  if(trap.x<MIND){
    t=trap.y;
    ro+=rd*t;
    col=Color(ro);
    vec3 N=normalize(vec3(-DE(ro-v.xyy)+DE(ro+v.xyy),-DE(ro-v.yxy)+DE(ro+v.yxy),-DE(ro-v.yyx)+DE(ro+v.yyx)));
    col.rgb=col.rgb*max(0.0,0.25+dot(N,L)*0.75)+col.a*max(0.0,dot(reflect(rd,N),L))*vec3(1.0,0.9,0.8);
    t=0.07;
    float sh=1.0;
    for(int i=0;i<SHADOWSTEPS;i++){
      t+=d=DE(ro+L*t)*1.25;
      sh=min(sh,d/t);
    }
    col.rgb*=clamp(sh*32.0,0.25,1.0);
  }
  if(rd.y<0.0)rd=mix(brd,vec3(rd.x,-rd.y,rd.z),1.0+rd.y*10.0);
  vec2 pt=vec2(atan(rd.z,rd.x)-0.5,rd.y*0.25+time*0.01);
  pt.x*=0.5;
  vec3 bcol=rd*0.1+vec3(0.6,0.7,1.0)*clamp(1.0-texture2D(tex,pt).r*0.5,0.75,1.0);
  bcol*=(1.0+rd.y);
  if(trap.x<MIND){
    col.rgb=mix(col.rgb,bcol,smoothstep(0.0,MIND,trap.x));
  }else col.rgb=bcol;
  return col.rgb;
}
mat3 lookat(vec3 fw,vec3 up){
  fw=normalize(fw);vec3 rt=normalize(cross(fw,normalize(up)));return mat3(rt,cross(rt,fw),fw);
}
void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
  L=normalize(vec3(0.5,0.5,-0.5));
  rmx=mat2(cos(-0.5),-sin(-0.5),sin(-0.5),cos(-0.5))*0.01;
  vec3 ro=vec3(4.222,0.0,-3.0);
  vec2 p=rmx*ro.xz+vec2(-time*0.05,0.0);
  ro.y=texture2D(tex,p.yx).g*0.5;
  mat3 cmx=lookat(vec3(2.8+sin(time+sin(time*3.0))*0.5,-1.0,0.0)-ro,vec3(0.0,1.0,0.0));
  vec3 rd=cmx*normalize(vec3((2.0*fragCoord.xy-size.xy)/size.y,1.0));
  vec3 col=scene(ro,rd);
  fragColor = vec4(clamp(col,0.0,1.0),1.0);
}
