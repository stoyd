void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
  vec2 uv = (fragCoord.xy / iResolution.xy)-vec2(0.5);
    uv.y*=-1.0;
    vec2 pt=uv;
    float d=dot(pt,pt)-sin(pt.x*4.0+7.4*cos(pt.y*6.0))*0.05;
    float t=iGlobalTime+20.0;
    t=sin(t*2.0);
    uv*=0.9-t*0.025;
    uv+=vec2(-t*t*iGlobalTime*0.015,t)*0.0125;
    float s=uv.y-0.1+sin(uv.x*4.0+7.4*cos(uv.x*6.0))*0.01;
    uv+=vec2(0.5);
    vec3 col=texture2D(iChannel0,uv).rgb;
    col=mix(col,vec3(1.5-uv.y),smoothstep(0.0,0.05,s));
    s=texture2D(iChannel1,uv+vec2(iGlobalTime)*vec2(0.1,-0.5)).r;
    col=mix(col,vec3(1.0),smoothstep(0.7,1.0,s));
    col=mix(col,vec3(1.0),smoothstep(0.0,0.2,d));

  fragColor = vec4(col,1.0);
}
