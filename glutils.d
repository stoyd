module glutils;
private:
import glbinds;
import arsd.color;
import arsd.png;


// ////////////////////////////////////////////////////////////////////////// //
/*
__gshared GLuint glLastUsedTexture = 0;

public void useTexture (GLuint tid) {
  pragma(inline, true);
  if (glLastUsedTexture != tid) {
    glLastUsedTexture = tid;
    glBindTexture(GL_TEXTURE_2D, tid);
  }
}

public void useTexture (Texture tex) { pragma(inline, true); useTexture(tex !is null ? tex.tid : 0); }
*/


// ////////////////////////////////////////////////////////////////////////// //
public final class Texture {
  GLuint tid;
  int width, height;

  // default: repeat, linear
  enum Option : int {
    repeat = 1,
    clamp = 2,
    linear = 3,
    nearest = 4,
    fp = 5, // create floating point texture
  }

  this (string fname, Option[] opts...) { loadPng(fname, opts); }
  this (int w, int h, bool clear, Option[] opts...) { createEmpty(w, h, clear, opts); }
  ~this () { clear(); }


  void clear () {
    if (tid) {
      //useTexture(tid);
      glBindTexture(GL_TEXTURE_2D, tid);
      glDeleteTextures(1, &tid);
      //useTexture(0);
      glBindTexture(GL_TEXTURE_2D, 0);
      tid = 0;
      width = 0;
      height = 0;
    }
  }

  void createEmpty (int w, int h, bool clear, Option[] opts...) {
    import core.stdc.stdlib : malloc, free;
    assert(w > 0);
    assert(h > 0);

    GLuint wrapOpt = GL_REPEAT;
    GLuint filterOpt = GL_LINEAR;
    GLuint ttype = GL_UNSIGNED_BYTE;
    foreach (immutable opt; opts) {
      switch (opt) with (Option) {
        case repeat: wrapOpt = GL_REPEAT; break;
        case clamp: wrapOpt = GL_CLAMP_TO_EDGE; break;
        case linear: filterOpt = GL_LINEAR; break;
        case nearest: filterOpt = GL_NEAREST; break;
        case fp: ttype = GL_FLOAT; break;
        default:
      }
    }

    glGenTextures(1, &tid);
    //useTexture(tid);
    glBindTexture(GL_TEXTURE_2D, tid);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapOpt);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapOpt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterOpt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterOpt);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    ubyte* ptr = null;
    scope(exit) if (ptr !is null) free(ptr);
    if (clear) {
      import core.stdc.string : memset;
      ptr = cast(ubyte*)malloc(w*h*4*(ttype == GL_UNSIGNED_BYTE ? 1 : 4));
      memset(ptr, 0, w*h*4);
    }
    glTexImage2D(GL_TEXTURE_2D, 0, (ttype == GL_FLOAT ? GL_RGBA16F : GL_RGBA), w, h, 0, GL_BGRA, ttype, ptr);
    width = w;
    height = h;
  }

  void loadPng (string fname, Option[] opts...) {
    clear();
    auto img = readPng(fname).getAsTrueColorImage;
    createEmpty(img.width, img.height, false, opts);
    glTexSubImage2D(GL_TEXTURE_2D, 0,  0, 0, img.width, img.height, GL_RGBA, GL_UNSIGNED_BYTE, img.imageData.bytes.ptr);
  }

  void activate () { if (tid) glBindTexture(GL_TEXTURE_2D, tid); }
  void deactivate () { if (tid) glBindTexture(GL_TEXTURE_2D, 0); }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class TextureCube {
  GLuint tid;
  int width, height;

  // default: repeat, linear
  enum Option : int {
    repeat = 1,
    clamp = 2,
    linear = 3,
    nearest = 4,
  }

  this (string fname, Option[] opts...) { loadPng(fname, opts); }
  this (int w, int h, Option[] opts...) { createEmpty(w, h, opts); }
  ~this () { clear(); }


  void clear () {
    if (tid) {
      glBindTexture(GL_TEXTURE_CUBE_MAP, tid);
      glDeleteTextures(1, &tid);
      glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
      tid = 0;
      width = 0;
      height = 0;
    }
  }

  void createEmpty (int w, int h, Option[] opts...) {
    import core.stdc.stdlib : malloc, free;
    assert(w > 0);
    assert(h > 0);

    GLuint wrapOpt = GL_CLAMP_TO_EDGE;
    GLuint filterOpt = GL_LINEAR;
    foreach (immutable opt; opts) {
      switch (opt) with (Option) {
        case repeat: wrapOpt = GL_REPEAT; break;
        case clamp: wrapOpt = GL_CLAMP_TO_EDGE; break;
        case linear: filterOpt = GL_LINEAR; break;
        case nearest: filterOpt = GL_NEAREST; break;
        default:
      }
    }

    glGenTextures(1, &tid);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tid);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, wrapOpt);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, wrapOpt);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, wrapOpt);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, filterOpt);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, filterOpt);

    ubyte* ptr = null;
    scope(exit) if (ptr !is null) free(ptr);
    {
      import core.stdc.string : memset;
      ptr = cast(ubyte*)malloc(w*h*4*4);
      memset(ptr, 0, w*h*4);
    }
    foreach (int idx; 0..6) {
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+idx, 0, GL_RGBA16F, w, h, 0, GL_BGRA, GL_FLOAT, ptr);
    }
    width = w;
    height = h;
  }

  void loadPng (string fname, Option[] opts...) {
    clear();
    TrueColorImage[6] img;
    foreach (int idx; 0..6) {
      import std.string : format;
      img[idx] = readPng(fname.format(idx)).getAsTrueColorImage;
      if (idx > 0) {
        if (img[idx].width != img[idx-1].width || img[idx].height != img[idx-1].height) {
          assert(0, "cubemap fucked: "~fname);
        }
      }
    }
    createEmpty(img[0].width, img[0].height, opts);
    foreach (int idx; 0..6) {
      glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+idx, 0,  0, 0, img[idx].width, img[idx].height, GL_RGBA, GL_UNSIGNED_BYTE, img[idx].imageData.bytes.ptr);
    }
  }

  void activate () { if (tid) glBindTexture(GL_TEXTURE_CUBE_MAP, tid); }
  void deactivate () { if (tid) glBindTexture(GL_TEXTURE_CUBE_MAP, 0); }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class FBO {
  int width;
  int height;
  GLuint fbo;
  Texture tex;

  this (Texture atex) { createWithTexture(atex); }

  this (int wdt, int hgt, bool clear=true) {
    createWithTexture(new Texture(wdt, hgt, clear));
  }

  void activate () { if (fbo) glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo); }
  void deactivate () { glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); }

private:
  void createWithTexture (Texture atex) {
    assert(atex !is null && atex.tid);

    tex = atex;
    glGenFramebuffersEXT(1, &fbo);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    // attach 2D texture to this FBO
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex.tid, 0);

    /*
    glGenRenderbuffersEXT(1, &fboDepthId);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, fboDepthId);
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, LightSize, LightSize);
    // attach depth buffer to FBO
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, fboDepthId);
    */

    {
      GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
      if (status != GL_FRAMEBUFFER_COMPLETE_EXT) assert(0, "framebuffer fucked!");
    }

    width = tex.width;
    height = tex.height;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct SVec2I { int x, y; }
public struct SVec3I { int x, y, z; }
public struct SVec4I { int x, y, z, w; }

public struct SVec2F { float x, y; }
public struct SVec3F { float x, y, z; }
public struct SVec4F { float x, y, z, w; }


public final class Shader {
  string shaderName;
  GLuint prg = 0;
  GLint[string] vars;

  this (string ashaderName, const(char)[] src) {
    shaderName = ashaderName;
    if (src.length > int.max) {
      import core.stdc.stdio : printf;
      printf("shader '%.*s' code too long!", cast(uint)ashaderName.length, ashaderName.ptr);
      assert(0);
    }
    auto shaderId = glCreateShader(GL_FRAGMENT_SHADER);
    auto sptr = src.ptr;
    GLint slen = cast(int)src.length;
    glShaderSource(shaderId, 1, &sptr, &slen);
    glCompileShader(shaderId);
    GLint success = 0;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
    if (!success) {
      import core.stdc.stdio : printf;
      import core.stdc.stdlib : malloc;
      GLint logSize = 0;
      glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logSize);
      auto logStrZ = cast(GLchar*)malloc(logSize);
      glGetShaderInfoLog(shaderId, logSize, null, logStrZ);
      printf("shader '%.*s' compilation fucked!\n%s\n", cast(uint)ashaderName.length, ashaderName.ptr, logStrZ);
      assert(0);
    }
    prg = glCreateProgram();
    glAttachShader(prg, shaderId);
    glLinkProgram(prg);
  }

  GLint varId(NT) (NT vname) if (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])) {
    GLint id = -1;
    if (vname.length > 0 && vname.length <= 128) {
      if (auto vi = vname in vars) {
        id = *vi;
      } else {
        char[129] buf = void;
        buf[0..vname.length] = vname[];
        buf[vname.length] = 0;
        id = glGetUniformLocation(prg, buf.ptr);
        //{ import core.stdc.stdio; printf("[%.*s.%s]=%i\n", cast(uint)shaderName.length, shaderName.ptr, buf.ptr, id); }
        static if (is(NT == immutable(char)[])) {
          vars[vname.idup] = id;
        } else {
          vars[vname.idup] = id;
        }
        if (id < 0) {
          import core.stdc.stdio : printf;
          printf("shader '%.*s': unknown variable '%.*s'\n", cast(uint)shaderName.length, shaderName.ptr, cast(uint)vname.length, vname.ptr);
        }
      }
    }
    return id;
  }

  // get unified var id
  GLint opIndex(NT) (NT vname) if (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])) {
    auto id = varId(vname);
    if (id < 0) {
      import core.stdc.stdio : printf;
      printf("shader '%.*s': unknown variable '%.*s'\n", cast(uint)shaderName.length, shaderName.ptr, cast(uint)vname.length, vname.ptr);
      assert(0);
    }
    return id;
  }

  private import std.traits;
  void opIndexAssign(T, NT) (in auto ref T v, NT vname)
  if (((isIntegral!T && T.sizeof <= 4) || (isFloatingPoint!T && T.sizeof == float.sizeof) || isBoolean!T ||
       is(T : SVec2I) || is(T : SVec3I) || is(T : SVec4I) ||
       is(T : SVec2F) || is(T : SVec3F) || is(T : SVec4F)) &&
      (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])))
  {
    auto id = varId(vname);
    if (id < 0) return;
    //{ import core.stdc.stdio; printf("setting '%.*s' (%d)\n", cast(uint)vname.length, vname.ptr, id); }
         static if (isIntegral!T || isBoolean!T) glUniform1i(id, cast(int)v);
    else static if (isFloatingPoint!T) glUniform1f(id, cast(float)v);
    else static if (is(SVec2I : T)) glUniform2i(id, cast(int)v.x, cast(int)v.y);
    else static if (is(SVec3I : T)) glUniform3i(id, cast(int)v.x, cast(int)v.y, cast(int)v.z);
    else static if (is(SVec4I : T)) glUniform4i(id, cast(int)v.x, cast(int)v.y, cast(int)v.z, cast(int)v.w);
    else static if (is(SVec2F : T)) glUniform2f(id, cast(float)v.x, cast(float)v.y);
    else static if (is(SVec3F : T)) glUniform3f(id, cast(float)v.x, cast(float)v.y, cast(float)v.z);
    else static if (is(SVec4F : T)) glUniform4f(id, cast(float)v.x, cast(float)v.y, cast(float)v.z, cast(float)v.w);
    else static assert(0, "wtf?!");
  }

  void activate () { if (prg) glUseProgram(prg); }
  void deactivate () { glUseProgram(0); }
}


// ////////////////////////////////////////////////////////////////////////// //
//private import std.traits;

public:
void exec(TO, TG) (TO obj, TG dg) if (is(typeof(() { dg(); })) && is(typeof(() { obj.activate(); obj.deactivate(); }))) {
  obj.activate();
  scope(exit) obj.deactivate();
  dg();
}


// ////////////////////////////////////////////////////////////////////////// //
void orthoCamera (int wdt, int hgt) {
  glMatrixMode(GL_PROJECTION); // for ortho camera
  glLoadIdentity();
  // left, right, bottom, top, near, far
  glOrtho(0, wdt, hgt, 0, -1, 1);
}


/*
// ofsx: >0: move right
// ofsy: >0: move down
void orthoCameraTr (int wdt, int hgt, float ofsx=0.0f, float ofsy=0.0f) {
  glMatrixMode(GL_PROJECTION); // for ortho camera
  glLoadIdentity();
  // left, right, bottom, top, near, far
  glOrtho(0, wdt, hgt, 0, -1, 1);
  glTranslatef(ofsx, ofsy, 0.0f);
}


void orthoCameraAtXY (int wdt, int hgt, float cx=0.0f, float cy=0.0f) {
  glMatrixMode(GL_PROJECTION); // for ortho camera
  glLoadIdentity();
  // left, right, bottom, top, near, far
  glOrtho(0, wdt, hgt, 0, -1, 1);
  glTranslatef(-cx, -cy, 0.0f);
}


void drawFullViewTexture (GLuint tid, int twdt, int thgt) {
  useTexture(tid);
  glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex2i(0, 0); // top-left
    glTexCoord2f(1, 0); glVertex2i(twdt, 0); // top-right
    glTexCoord2f(1, 1); glVertex2i(twdt, thgt); // bottom-right
    glTexCoord2f(0, 1); glVertex2i(0, thgt); // bottom-left
  glEnd();
}


void drawFullViewTextureUD (GLuint tid, int twdt, int thgt) {
  useTexture(tid);
  glBegin(GL_QUADS);
    glTexCoord2f(0, 1); glVertex2i(0, 0); // top-left
    glTexCoord2f(1, 1); glVertex2i(twdt, 0); // top-right
    glTexCoord2f(1, 0); glVertex2i(twdt, thgt); // bottom-right
    glTexCoord2f(0, 0); glVertex2i(0, thgt); // bottom-left
  glEnd();
}


void drawFullViewTextureSS (GLuint tid, int twdt, int thgt) {
  useTexture(tid);
  glBegin(GL_QUADS);
    glTexCoord2f(1, 0); glVertex2i(0, 0); // top-left
    glTexCoord2f(0, 0); glVertex2i(twdt, 0); // top-right
    glTexCoord2f(0, 1); glVertex2i(twdt, thgt); // bottom-right
    glTexCoord2f(1, 1); glVertex2i(0, thgt); // bottom-left
  glEnd();
}


void drawFullViewTexture (Texture tex) {
  if (tex !is null && tex.tid) drawFullViewTexture(tex.tid, tex.width, tex.height);
}


void drawFullViewTextureUD (Texture tex) {
  if (tex !is null && tex.tid) drawFullViewTextureUD(tex.tid, tex.width, tex.height);
}
*/


// origin is texture left top
void drawAtXY (GLuint tid, int x, int y, int w, int h, bool mirrorX=false, bool mirrorY=false) {
  if (!tid || w < 1 || h < 1) return;
  w += x;
  h += y;
  if (mirrorX) { int tmp = x; x = w; w = tmp; }
  if (mirrorY) { int tmp = y; y = h; h = tmp; }
  glBindTexture(GL_TEXTURE_2D, tid);
  glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex2i(x, y); // top-left
    glTexCoord2f(1.0f, 0.0f); glVertex2i(w, y); // top-right
    glTexCoord2f(1.0f, 1.0f); glVertex2i(w, h); // bottom-right
    glTexCoord2f(0.0f, 1.0f); glVertex2i(x, h); // bottom-left
  glEnd();
}


// origin is texture center
void drawAtXYC (Texture tex, int x, int y, bool mirrorX=false, bool mirrorY=false) {
  if (tex is null || !tex.tid) return;
  x -= tex.width/2;
  y -= tex.height/2;
  drawAtXY(tex.tid, x, y, tex.width, tex.height, mirrorX, mirrorY);
}


// origin is texture left top
void drawAtXY (Texture tex, int x, int y, bool mirrorX=false, bool mirrorY=false) {
  if (tex is null || !tex.tid) return;
  drawAtXY(tex.tid, x, y, tex.width, tex.height, mirrorX, mirrorY);
}
