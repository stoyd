module glbinds;

static if (__traits(compiles, () { import arsd.simpledisplay; })) {
  public import arsd.simpledisplay;
} else {
  public import simpledisplay;
}


// ////////////////////////////////////////////////////////////////////////// //
extern(C) nothrow @nogc:

alias GLboolean = ubyte;
alias GLuint = uint;
alias GLenum = uint;
alias GLchar = char;
alias GLsizei = int;
alias GLfloat = float;

enum uint GL_ONE = 1;
enum uint GL_RGBA8 = 0x8058;
enum uint GL_FRAGMENT_SHADER = 0x8B30;
enum uint GL_COMPILE_STATUS = 0x8B81;
enum uint GL_INFO_LOG_LENGTH = 0x8B84;
enum uint GL_FRAMEBUFFER_COMPLETE_EXT = 0x8CD5;
enum uint GL_FRAMEBUFFER_EXT = 0x8D40;
enum uint GL_COLOR_ATTACHMENT0_EXT = 0x8CE0;
enum uint GL_RENDERBUFFER_EXT = 0x8D41;
enum uint GL_DEPTH_COMPONENT16 = 0x81A5;
enum uint GL_DEPTH_COMPONENT24 = 0x81A6;
enum uint GL_DEPTH_COMPONENT32 = 0x81A7;
enum uint GL_DEPTH_ATTACHMENT_EXT = 0x8D00;
enum uint GL_TEXTURE0 = 0x84C0;
enum uint GL_TEXTURE1 = 0x84C1;
enum uint GL_TEXTURE2 = 0x84C2;
enum uint GL_TEXTURE3 = 0x84C3;
enum uint GL_TEXTURE4 = 0x84C4;
enum uint GL_TEXTURE5 = 0x84C5;
enum uint GL_TEXTURE6 = 0x84C6;
enum uint GL_TEXTURE7 = 0x84C7;
enum uint GL_TEXTURE8 = 0x84C8;
enum uint GL_TEXTURE9 = 0x84C9;
enum uint GL_TEXTURE10 = 0x84CA;
enum uint GL_TEXTURE11 = 0x84CB;
enum uint GL_TEXTURE12 = 0x84CC;
enum uint GL_TEXTURE13 = 0x84CD;
enum uint GL_TEXTURE14 = 0x84CE;
enum uint GL_TEXTURE15 = 0x84CF;
enum uint GL_TEXTURE16 = 0x84D0;
enum uint GL_TEXTURE17 = 0x84D1;
enum uint GL_TEXTURE18 = 0x84D2;
enum uint GL_TEXTURE19 = 0x84D3;
enum uint GL_TEXTURE20 = 0x84D4;
enum uint GL_TEXTURE21 = 0x84D5;
enum uint GL_TEXTURE22 = 0x84D6;
enum uint GL_TEXTURE23 = 0x84D7;
enum uint GL_TEXTURE24 = 0x84D8;
enum uint GL_TEXTURE25 = 0x84D9;
enum uint GL_TEXTURE26 = 0x84DA;
enum uint GL_TEXTURE27 = 0x84DB;
enum uint GL_TEXTURE28 = 0x84DC;
enum uint GL_TEXTURE29 = 0x84DD;
enum uint GL_TEXTURE30 = 0x84DE;
enum uint GL_TEXTURE31 = 0x84DF;
enum uint GL_ACTIVE_TEXTURE = 0x84E0;
enum uint GL_COMPILE = 0x1300;
enum uint GL_COMPILE_AND_EXECUTE = 0x1301;
enum uint GL_TEXTURE_CUBE_MAP = 0x8513;
enum uint GL_TEXTURE_WRAP_R = 0x8072;
enum uint GL_TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515;
enum uint GL_TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516;
enum uint GL_TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517;
enum uint GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518;
enum uint GL_TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519;
enum uint GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A;
enum uint GL_RGBA16F = 0x881A;
enum uint GL_FLOAT = 0x1406;
enum uint GL_DOUBLE = 0x140A;

GLuint glCreateShader (GLenum);
void glShaderSource (GLuint, GLsizei, const(GLchar*)*, const(GLint)*);
void glCompileShader (GLuint);
GLuint glCreateProgram ();
void glAttachShader (GLuint, GLuint);
void glLinkProgram (GLuint);
void glUseProgram (GLuint);
void glGetShaderiv (GLuint, GLenum, GLint*);
void glGetShaderInfoLog (GLuint, GLsizei, GLsizei*, GLchar*);

GLint glGetUniformLocation (GLuint, const(GLchar)*);

void glUniform1f (GLint, GLfloat);
void glUniform2f (GLint, GLfloat, GLfloat);
void glUniform3f (GLint, GLfloat, GLfloat, GLfloat);
void glUniform4f (GLint, GLfloat, GLfloat, GLfloat, GLfloat);
void glUniform1i (GLint, GLint);
void glUniform2i (GLint, GLint, GLint);
void glUniform3i (GLint, GLint, GLint, GLint);
void glUniform4i (GLint, GLint, GLint, GLint, GLint);
void glUniform1fv (GLint, GLsizei, const(GLfloat)*);
void glUniform2fv (GLint, GLsizei, const(GLfloat)*);
void glUniform3fv (GLint, GLsizei, const(GLfloat)*);
void glUniform4fv (GLint, GLsizei, const(GLfloat)*);
void glUniform1iv (GLint, GLsizei, const(GLint)*);
void glUniform2iv (GLint, GLsizei, const(GLint)*);
void glUniform3iv (GLint, GLsizei, const(GLint)*);
void glUniform4iv (GLint, GLsizei, const(GLint)*);
void glUniformMatrix2fv (GLint, GLsizei, GLboolean, const(GLfloat)*);
void glUniformMatrix3fv (GLint, GLsizei, GLboolean, const(GLfloat)*);
void glUniformMatrix4fv (GLint, GLsizei, GLboolean, const(GLfloat)*);

void glGenFramebuffersEXT (GLsizei, GLuint*);
void glBindFramebufferEXT (GLenum, GLuint);
void glFramebufferTexture2DEXT (GLenum, GLenum, GLenum, GLuint, GLint);
void glGenRenderbuffersEXT (GLsizei, GLuint*);
void glRenderbufferStorageEXT (GLenum, GLenum, GLsizei, GLsizei);
void glFramebufferRenderbufferEXT (GLenum, GLenum, GLenum, GLuint);
GLenum glCheckFramebufferStatusEXT (GLenum);
void glBindRenderbufferEXT (GLenum, GLuint);

void glActiveTexture (GLenum);

GLuint glGenLists (GLsizei);
void glNewList (GLuint, GLenum);
void glEndList ();
void glCallList (GLuint);
void glCallLists (GLsizei, GLenum, const(void)*);
void glDeleteLists (GLuint, GLsizei);
